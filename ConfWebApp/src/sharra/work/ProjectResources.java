package sharra.work;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

@ManagedBean
@SessionScoped
public class ProjectResources {

	List<String> presentersOptions;
	List<String> roomsOptions;

	List<Presenter> presenters;
	List<Room> rooms;
	private Controller controller;

	public ProjectResources() {
		getPresenterOptions();
	}

	public ProjectResources(List<String> presentersOptions, List<String> roomsOptions, List<Presenter> presenters,
			List<Room> rooms) {
		this.presentersOptions = presentersOptions;
		this.roomsOptions = roomsOptions;
		this.presenters = presenters;
		this.rooms = rooms;
	}
	
	//get the presenter list array for populating. 
	public List<String> getPresenterOptions() {
		presenters = controller.getPresenters();
		
		for(int i=0; i<presenters.size(); i++) {
			presentersOptions.add(presenters.get(i).name);
		}
		return presentersOptions;
		}
	
	//get the id of the presenter for assignment
	public int getPresenterId(String presenterName) {
		int presenterId = -1;
		
		while (presenterId != -1) {
			
			for (int i=0; i<presenters.size(); i++) {
				if (presenterName == presenters.get(i).name) {
					presenterId = presenters.get(i).id;
				}
			}
			break;
		}
		//if id is not found for some weird reason set to default presenter. 
		
		if (presenterId == -1) {
			presenterId=1;
		}
		return presenterId; 
	}


	public List<String> getPresentersOptions() {
		return presentersOptions;
	}

	public void setPresentersOptions(List<String> presentersOptions) {
		this.presentersOptions = presentersOptions;
	}

	public List<String> getRoomsOptions() {
		return roomsOptions;
	}

	public void setRoomsOptions(List<String> roomsOptions) {
		this.roomsOptions = roomsOptions;
	}

	public List<Presenter> getPresenters() {
		return presenters;
	}

	public void setPresenters(List<Presenter> presenters) {
		this.presenters = presenters;
	}

	public List<Room> getRooms() {
		return rooms;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}

	
	
}
