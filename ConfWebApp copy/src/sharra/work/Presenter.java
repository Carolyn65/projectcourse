package sharra.work;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Base64;
import java.util.Scanner;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.imageio.ImageIO;
import javax.servlet.http.Part;
import javax.sql.rowset.serial.SerialException;



@ManagedBean
@SessionScoped
public class Presenter {
	int id;
	String name;
	Blob picture;
	byte[] pictureArr;
	String bio;
	String imageString;
	Part file;
	private Logger logger = Logger.getLogger(getClass().getName());
	
	
	
	public Presenter(){}

	public Presenter(int id, String name, Blob picture, byte[] pictureArr, String bio) throws Exception {
		super();
		this.id = id;
		this.name = name;
		this.pictureArr=pictureArr;
		this.picture = picture; 
		this.bio = bio;
		
		try {
			imageString= new String(Base64.getEncoder().encodeToString(pictureArr));
			
		} catch (Exception e) {
		    logger.info("Error converting picture");
		}
		
	}
	
	public void validate(FacesContext context, UIComponent  component, Object value) {
		logger.info("validate method called");
		Part file = (Part) value;
		if(file.getSize() > 12000000) {
			throw new ValidatorException(new FacesMessage("File is too large"));
		}
		if(!file.getContentType().endsWith("jpg")) {
		}
		else throw new ValidatorException(new FacesMessage("File is not a jpg file"));
		
	}
	
	public String upload() throws IOException, SerialException, SQLException {
	logger.info("upload method called");
	Scanner s = new Scanner(file.getInputStream());
	String fileContent = s.useDelimiter("\\A").next(); // this is the contents of uploaded file. Should be instead shoved to a byte array
	//imageString= new String(Base64.getEncoder().encodeToString(pictureArr));
	Files.write(Paths.get("c:\\"+file.getSubmittedFileName()),fileContent.getBytes(), StandardOpenOption.CREATE);
	//pictureArr = fileContent.getBytes();
	//picture = new javax.sql.rowset.serial.SerialBlob(pictureArr);
	s.close();
	return "edit_presenter?faces-redirect=true";
	
	}

	public Part getFile() {
		return file;
	}

	public void setFile(Part file) {
		this.file = file;
	}

	public String getImageString() {
		return imageString;
	}

	public void setImageString(String imageString) {
		this.imageString = imageString;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public byte[] getPictureArr() {
		return pictureArr;
	}

	public void setPictureArr(byte[] pictureArr) {
		this.pictureArr = pictureArr;
	}

	public Blob getPicture() {
		//Call convert to byte array and picture method. 
		return picture;
	}

	public void setPicture(Blob picture) {
		//Setup second set method for Admin to convert picture to blob
		this.picture = picture;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}


	@Override
	public String toString() {
		return "Presenter [id=" + id + ", name=" + name + ", bio=" + bio + "]";
	}
	
	

}
