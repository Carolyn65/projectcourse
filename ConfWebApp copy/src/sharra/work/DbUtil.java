package sharra.work;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.sql.SQLException;
import java.sql.DriverManager;


import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DbUtil {

	private static DbUtil instance;
	private DataSource dataSource;
	private String jndiName = "java:comp/env/jdbc/cp4898_conventionapp";
	private Logger logger = Logger.getLogger(getClass().getName());

	public boolean validate(String user, String password) {
		logger.info("Made it to dbController");
		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = getConnection();
			logger.info("Obtained connection");
			ps = con.prepareStatement("Select userId, password from admin_users where userId = ? and password = ?");
			logger.info("post prepared statement");
			ps.setString(1, user);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			logger.info("sql statement executed and returned");
			if (rs.next()) {
				//result found, means valid inputs
				return true;
			}
		} catch (SQLException ex) {
			System.out.println("Login error -->" + ex.getMessage());
			return false;
		} finally {
			close(con);
		}
		return false;
	}
	
//	public static Connection getConnection() {
//		try {
//			Class.forName("com.mysql.jdbc.Driver");
//			Connection con = DriverManager.getConnection(
//					"jdbc:mysql://localhost:3306/cardb", "pankaj", "pankaj123");
//			return con;
//		} catch (Exception ex) {
//			System.out.println("Database.getConnection() Error -->"
//					+ ex.getMessage());
//			return null;
//		}
//	}
	
	private Connection getConnection() {

		try {
			Connection theConn = dataSource.getConnection();
			return theConn;
		}catch(Exception ex){
			System.out.println("Database.getConnection() Error -->"
					+ ex.getMessage());
			return null;
		}

		
	}
	
	public static DbUtil getInstance() throws Exception {
		if (instance == null) {
			instance = new DbUtil();
		}

		return instance;
	}

	private DbUtil() throws Exception {
		dataSource = getDataSource();
	}

	private DataSource getDataSource() throws NamingException {
		Context context = new InitialContext();

		DataSource theDataSource = (DataSource) context.lookup(jndiName);

		return theDataSource;
	}



	private void close(Connection theConn, Statement theStmt) {
		close(theConn, theStmt, null);
	}

	private void close(Connection theConn, Statement theStmt, ResultSet theRs) {

		try {
			if (theRs != null) {
				theRs.close();
			}

			if (theStmt != null) {
				theStmt.close();
			}

			if (theConn != null) {
				theConn.close();
			}

		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}

	public List<Panel> getPanels() throws Exception {

		List<Panel> panels = new ArrayList<>();

		Connection myConn = null;
		Statement myStmt = null;
		ResultSet myRs = null;

		try {
			myConn = getConnection();

			String sql = "select * from panels order by dateTime";
			myStmt = myConn.createStatement();
			myRs = myStmt.executeQuery(sql);

			// process result set
			while (myRs.next()) {

				// retrieve data from result set row
				int id = myRs.getInt("id");
				String name = myRs.getString("name");
				int presenter = myRs.getInt("presenter");
				String description = myRs.getString("description");
				int room = myRs.getInt("room");
				LocalDateTime dateTime = myRs.getObject("dateTime", LocalDateTime.class);//may have issues as not sure this is the format it comes in from database as 
				int panelRating = myRs.getInt("panelRating");

				
				//get & create Room
					Connection tempConn = getConnection();
					String sqlTemp = "select * from rooms where id=?";
					PreparedStatement myStmtTemp = tempConn.prepareStatement(sqlTemp);
					
					//Set params 
					myStmtTemp.setInt(1, room);
					Room tempRoom = null;
					
					ResultSet myRsTemp = myStmtTemp.executeQuery();
					while(myRsTemp.next()) {
						int roomId = myRsTemp.getInt("id");
						String roomDescription = myRsTemp.getString("description");
						int gpsLoc = myRsTemp.getInt("gpsLoc");
						tempRoom = new Room(roomId, roomDescription, gpsLoc);

					}
						
				//get & create Presenter
						Connection tempConn2 = getConnection();
						String sqlTemp2 = "select * from presenters where id=?";
						PreparedStatement myStmtTemp2 = tempConn.prepareStatement(sqlTemp2);
						//Set params 
						myStmtTemp2.setInt(1, presenter);
						
						ResultSet myRsTemp2 = myStmtTemp2.executeQuery();
						Presenter tempPresenter = null;
						while(myRsTemp2.next())
						{							
							int presenterId = myRsTemp2.getInt("id");
							String presenterName = myRsTemp2.getString("name");
							byte[] pictureArr = myRs.getBytes("picture");
							Blob presenterPicture = myRsTemp2.getBlob("picture");
							String bio = myRsTemp2.getString("bio");
							tempPresenter = new Presenter(presenterId, presenterName, presenterPicture, pictureArr, bio);
							
						}

				// create new Panel object
				
				Panel tempPanel = new Panel(id, name, tempPresenter, description, tempRoom, dateTime, panelRating);
				logger.info("Panel success");
				

				// add it to the list of students
				panels.add(tempPanel);
				close(tempConn, myStmtTemp, myRsTemp);
				close(tempConn2, myStmtTemp2, myRsTemp2);
			}

			return panels;
		} finally {
			close(myConn, myStmt, myRs);
		}
	}
	
	public List<Presenter> getPresenters() throws Exception {

		List<Presenter> presenters = new ArrayList<>();

		Connection myConn = null;
		Statement myStmt = null;
		ResultSet myRs = null;

		try {
			myConn = getConnection();

			String sql = "select * from presenters order by name";
			myStmt = myConn.createStatement();
			myRs = myStmt.executeQuery(sql);

			// process result set
			while (myRs.next()) {

				// retrieve data from result set row
				int presenterId = myRs.getInt("id");
				String presenterName = myRs.getString("name");
				byte[] pictureArr = myRs.getBytes("picture");
				Blob presenterPicture = myRs.getBlob("picture");
				String bio = myRs.getString("bio");
				Presenter tempPresenter= new Presenter(presenterId, presenterName, presenterPicture, pictureArr, bio);
				
				logger.info("Presenter success");
				
				presenters.add(tempPresenter);
				
			}

			return presenters;
		} finally {
			close(myConn, myStmt, myRs);
		}
	}

	
	public List<Room> getRooms() throws Exception {

		List<Room> rooms = new ArrayList<>();

		Connection myConn = null;
		Statement myStmt = null;
		ResultSet myRs = null;

		try {
			myConn = getConnection();

			String sql = "select * from rooms order by description";
			myStmt = myConn.createStatement();
			myRs = myStmt.executeQuery(sql);

			// process result set
			while (myRs.next()) {

				// retrieve data from result set row
				int roomId = myRs.getInt("id");
				String roomDescription = myRs.getString("description");
				int gpsLoc = myRs.getInt("gpsLoc");
				
				Room tempRoom = new Room(roomId, roomDescription, gpsLoc);
				
				logger.info("Room success");
				
				rooms.add(tempRoom);
				
			}

			return rooms;
		} finally {
			close(myConn, myStmt, myRs);
		}
	}
	
	public Panel getPanel(int panelId) throws Exception {

		Connection myConn = null;
		PreparedStatement myStmt = null;
		ResultSet myRs = null;

		try {
			myConn = getConnection();

			String sql = "select * from panels where id=?";

			myStmt = myConn.prepareStatement(sql);

			// set params
			myStmt.setInt(1, panelId);

			myRs = myStmt.executeQuery();

			Panel thePanel = null;

			// retrieve data from result set row
			if (myRs.next()) {
				// retrieve data from result set row
				int id = myRs.getInt("id");
				String name = myRs.getString("name");
				int presenter = myRs.getInt("presenter");
				String description = myRs.getString("description");
				int room = myRs.getInt("room");
				LocalDateTime dateTime = myRs.getObject("dateTime", LocalDateTime.class);//may have issues as not sure this is the format it comes in from database as 
				int panelRating = myRs.getInt("panelRating");

				
				//get & create Room
					Connection tempConn = getConnection();
					String sqlTemp = "select * from rooms where id=?";
					PreparedStatement myStmtTemp = tempConn.prepareStatement(sqlTemp);
					
					//Set params 
					myStmtTemp.setInt(1, room);
					Room tempRoom = null;
					
					ResultSet myRsTemp = myStmtTemp.executeQuery();
					while(myRsTemp.next()) {
						int roomId = myRsTemp.getInt("id");
						String roomDescription = myRsTemp.getString("description");
						int gpsLoc = myRsTemp.getInt("gpsLoc");
						tempRoom = new Room(roomId, roomDescription, gpsLoc);

					}
						
				//get & create Presenter
						Connection tempConn2 = getConnection();
						String sqlTemp2 = "select * from presenters where id=?";
						PreparedStatement myStmtTemp2 = tempConn.prepareStatement(sqlTemp2);
						//Set params 
						myStmtTemp2.setInt(1, presenter);
						
						ResultSet myRsTemp2 = myStmtTemp2.executeQuery();
						Presenter tempPresenter = null;
						while(myRsTemp2.next())
						{							
							int presenterId = myRsTemp2.getInt("id");
							String presenterName = myRsTemp2.getString("name");
							byte[] pictureArr = myRs.getBytes("picture");
							Blob presenterPicture = myRsTemp2.getBlob("picture");
							String bio = myRsTemp2.getString("bio");
							tempPresenter = new Presenter(presenterId, presenterName, presenterPicture, pictureArr, bio);
					
						}

				// create new Panel object
				
				thePanel = new Panel(id, name, tempPresenter, description, tempRoom, dateTime, panelRating);
				logger.info("Panel success");
				
			} else {
				throw new Exception("Could not find panel id: " + panelId);
			}
			return thePanel;
		} finally {
			close(myConn, myStmt, myRs);
		}
	}

	public Presenter getPresenter(int presenterId) throws Exception {

		Connection myConn = null;
		PreparedStatement myStmt = null;
		ResultSet myRs = null;

		try {
			myConn = getConnection();

			String sql = "select * from presenters where id=?";

			myStmt = myConn.prepareStatement(sql);

			// set params
			myStmt.setInt(1, presenterId);

			myRs = myStmt.executeQuery();

			Presenter thePresenter = null;

			// retrieve data from result set row
			if (myRs.next()) {
				// retrieve data from result set row
										
			int Id = myRs.getInt("id");
			String presenterName = myRs.getString("name");
			byte[] pictureArr = myRs.getBytes("picture");
			Blob presenterPicture = myRs.getBlob("picture");
			String bio = myRs.getString("bio");
			thePresenter= new Presenter(Id, presenterName, presenterPicture, pictureArr, bio);
							
				logger.info("Presenter success");
				
			} else {
				throw new Exception("Could not find presenter id: " + presenterId);
			}

			return thePresenter;
		} finally {
			close(myConn, myStmt, myRs);
		}
	}
	
	public void addPanel(Panel thePanel) throws Exception {

		Connection myConn = null;
		PreparedStatement myStmt = null;

		try {
			myConn = getConnection();

			String sql = "insert into panels (name, presenter, description, room, dateTime, panelRating) values (?, ?, ?, ?, ?, ?)";

			myStmt = myConn.prepareStatement(sql);

			myStmt = myConn.prepareStatement(sql);
			logger.info("updating panel panels db : " + thePanel);
			// set params
			myStmt.setString(1, thePanel.getName());
			myStmt.setInt(2, thePanel.getPresenterId());
			myStmt.setString(3, thePanel.getDescription());
			myStmt.setInt(4, thePanel.getRoomId());
			myStmt.setString(5, thePanel.getDateStr()); 
			myStmt.setInt(6, thePanel.getPanelRating());
				
			myStmt.execute();

			
		} finally {
			close(myConn, myStmt);
		}

	}

	public void updatePanel(Panel thePanel) throws Exception {

		Connection myConn = null;
		PreparedStatement myStmt = null;
		

		try {
			myConn = getConnection();
			//readd dateTime to the sql query once sorted
			String sql = "update panels " + " set name=?, presenter=?, description=?, room=?, dateTime=?, panelRating=?" + " where id=?";
			

			myStmt = myConn.prepareStatement(sql);
			logger.info("updating panel panels db : " + thePanel);
			// set params
			myStmt.setString(1, thePanel.getName());
			myStmt.setInt(2, thePanel.getPresenterId());
			myStmt.setString(3, thePanel.getDescription());
			myStmt.setInt(4, thePanel.getRoomId());
			myStmt.setString(5, thePanel.getDateStr()); //needs fixed for date & time
			myStmt.setInt(6, thePanel.getPanelRating());
			myStmt.setInt(7, thePanel.getId());

			myStmt.execute();
		} finally {
			close(myConn, myStmt);
		}

	}
	
	public void updatePresenter(Presenter thePresenter) throws Exception {

		Connection myConn = null;
		PreparedStatement myStmt = null;
		

		try {
			myConn = getConnection();
			//readd dateTime to the sql query once sorted
			String sql = "update presenters " + " set name=?, picture=?, bio=?" + " where id=?";
			

			myStmt = myConn.prepareStatement(sql);
			logger.info("updating panel panels db : " + thePresenter);
			// set params
			myStmt.setString(1, thePresenter.getName());
			myStmt.setBlob(2, thePresenter.getPicture());
			myStmt.setString(3, thePresenter.getBio());
			myStmt.setInt(4, thePresenter.getId());

			myStmt.execute();
		} finally {
			close(myConn, myStmt);
		}

	}
//
	public void deletePanel(int panelId) throws Exception {

		Connection myConn = null;
		PreparedStatement myStmt = null;
		
		try {
			myConn = getConnection();

			String sql = "delete from schedule where panel_id=?";

			myStmt = myConn.prepareStatement(sql);

			// set params
			myStmt.setInt(1, panelId);

			myStmt.execute();
		} finally {
			close(myConn, myStmt);
		}

		try {
			myConn = getConnection();

			String sql = "delete from panels where id=?";

			myStmt = myConn.prepareStatement(sql);

			// set params
			myStmt.setInt(1, panelId);

			myStmt.execute();
		} finally {
			close(myConn, myStmt);
		}
	}
	
	public void deletePresenter(int presenterId) throws Exception {

		Connection myConn = null;
		PreparedStatement myStmt = null;
				
		try {
			myConn = getConnection();

			String sql = "delete from panels where presenter=?";

			myStmt = myConn.prepareStatement(sql);

			// set params
			myStmt.setInt(1, presenterId);

			myStmt.execute();
		} finally {
			close(myConn, myStmt);
		}

		try {
			myConn = getConnection();

			String sql = "delete from presenters where id=?";

			myStmt = myConn.prepareStatement(sql);

			// set params
			myStmt.setInt(1, presenterId);

			myStmt.execute();
		} finally {
			close(myConn, myStmt);
		}
	}
//
	// Search method ----------------------------------------------------------------
	public List<Panel> searchPanels(String theSearchName) throws Exception {

		// 1- Result list
		List<Panel> panels = new ArrayList<>();

		// 2- Clean attributes
		Connection myConn = null;
		PreparedStatement myStmt = null;
		ResultSet myRs = null;

		try {

			// 3- Get connection to database
			myConn = dataSource.getConnection();

			// 4- Only search by name if theSearchName is not empty
			if (theSearchName != null && theSearchName.trim().length() > 0) {

				// 5- Create sql to search for students by name
				String sql = "select * from panels where lower(name) like ? or lower(description) like ?";

				// 6- Create prepared statement
				myStmt = myConn.prepareStatement(sql);

				// 7- Set params
				String theSearchNameLike = "%" + theSearchName.toLowerCase() + "%";
				myStmt.setString(1, theSearchNameLike);
				myStmt.setString(2, theSearchNameLike);

			} else {
				// 8- Create sql to get all students
				String sql = "select * from panels order by dateTime";

				// 9- Create prepared statement
				myStmt = myConn.prepareStatement(sql);
			}

			// 10- Execute statement
			myRs = myStmt.executeQuery();

			// 11- Retrieve data from result set row
			while (myRs.next()) {

				// 12- Retrieve data from result set row
				// retrieve data from result set row
				int id = myRs.getInt("id");
				String name = myRs.getString("name");
				int presenter = myRs.getInt("presenter");
				String description = myRs.getString("description");
				int room = myRs.getInt("room");
				LocalDateTime dateTime = myRs.getObject("dateTime", LocalDateTime.class);//may have issues as not sure this is the format it comes in from database as 
				int panelRating = myRs.getInt("panelRating");

				
				//get & create Room
					Connection tempConn = getConnection();
					String sqlTemp = "select * from rooms where id=?";
					PreparedStatement myStmtTemp = tempConn.prepareStatement(sqlTemp);
					
					//Set params 
					myStmtTemp.setInt(1, room);
					Room tempRoom = null;
					
					ResultSet myRsTemp = myStmtTemp.executeQuery();
					while(myRsTemp.next()) {
						int roomId = myRsTemp.getInt("id");
						String roomDescription = myRsTemp.getString("description");
						int gpsLoc = myRsTemp.getInt("gpsLoc");
						tempRoom = new Room(roomId, roomDescription, gpsLoc);

					}
						
				//get & create Presenter
						Connection tempConn2 = getConnection();
						String sqlTemp2 = "select * from presenters where id=?";
						PreparedStatement myStmtTemp2 = tempConn.prepareStatement(sqlTemp2);
						//Set params 
						myStmtTemp2.setInt(1, presenter);
						
						ResultSet myRsTemp2 = myStmtTemp2.executeQuery();
						Presenter tempPresenter = null;
						while(myRsTemp2.next())
						{							
							int presenterId = myRsTemp2.getInt("id");
							String presenterName = myRsTemp2.getString("name");
							byte[] pictureArr = myRs.getBytes("picture");
							Blob presenterPicture = myRsTemp2.getBlob("picture");
							String bio = myRsTemp2.getString("bio");
							tempPresenter = new Presenter(presenterId, presenterName, presenterPicture, pictureArr, bio);
							
						}

				// create new Panel object
				
				Panel tempPanel = new Panel(id, name, tempPresenter, description, tempRoom, dateTime, panelRating);
				logger.info("Panel success");

				panels.add(tempPanel);
			}

			return panels;
		} finally {

			// 15- Clean up JDBC objects
			close(myConn, myStmt, myRs);
		}
	}
	
	public List<Presenter> searchPresenters(String theSearchNamePresenters) throws Exception {

		// 1- Result list
		List<Presenter> presenters = new ArrayList<>();

		// 2- Clean attributes
		Connection myConn = null;
		PreparedStatement myStmt = null;
		ResultSet myRs = null;

		try {

			// 3- Get connection to database
			myConn = dataSource.getConnection();

			// 4- Only search by name if theSearchNamePresenters is not empty
			if (theSearchNamePresenters != null && theSearchNamePresenters.trim().length() > 0) {

				// 5- Create sql to search for presenter by name
				String sql = "select * from presenters where lower(name) like ? or lower(bio) like ?";

				// 6- Create prepared statement
				myStmt = myConn.prepareStatement(sql);

				// 7- Set params
				String theSearchNameLike = "%" + theSearchNamePresenters.toLowerCase() + "%";
				myStmt.setString(1, theSearchNameLike);
				myStmt.setString(2, theSearchNameLike);

			} else {
				// 8- Create sql to get all presenters
				String sql = "select * from presenters order by name";

				// 9- Create prepared statement
				myStmt = myConn.prepareStatement(sql);
			}

			// 10- Execute statement
			myRs = myStmt.executeQuery();

			// 11- Retrieve data from result set row
			while (myRs.next()) {
						
							int presenterId = myRs.getInt("id");
							String presenterName = myRs.getString("name");
							byte[] pictureArr = myRs.getBytes("picture");
							Blob presenterPicture = myRs.getBlob("picture");
							String bio = myRs.getString("bio");							

				// create new Presenter object
				
				Presenter tempPresenter = new Presenter(presenterId, presenterName, presenterPicture, pictureArr, bio);
				logger.info("Presenter success");
			

				presenters.add(tempPresenter);
			}

			return presenters;
		} finally {

			// 15- Clean up JDBC objects
			close(myConn, myStmt, myRs);
		}
	}
	
	public static void close(Connection con) {
		try {
			con.close();
		} catch (Exception ex) {
		}
	}
	// ------------------------------------------------------------------------------

}
