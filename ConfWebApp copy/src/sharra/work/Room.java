package sharra.work;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped

public class Room {
	int id;
	String description;
	int gpsLoc;
	public Room() {
	}
	public Room(int id, String description, int gpsLoc) {
		this.id = id;
		this.description = description;
		this.gpsLoc = gpsLoc;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getGpsLoc() {
		return gpsLoc;
	}
	public void setGpsLoc(int gpsLoc) {
		this.gpsLoc = gpsLoc;
	}
	@Override
	public String toString() {
		return "Room [description= + description]";
	}

	

}
