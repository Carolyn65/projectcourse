package hellobobby.example.com.android_project.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;
import hellobobby.example.com.android_project.R;
import hellobobby.example.com.android_project.entities.Panel;
import hellobobby.example.com.android_project.entities.Presenter;
import hellobobby.example.com.android_project.entities.Schedule;
import hellobobby.example.com.android_project.services.ConventionAPI;
import hellobobby.example.com.android_project.services.Globals;
import hellobobby.example.com.android_project.services.InitData;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static hellobobby.example.com.android_project.activities.PanelListActivity.panels;

public class UserScheduleActivity extends AppCompatActivity {
    TextView tv_scheduleItem_name;
    TextView tv_scheduleItem_time;
    GridView gv_schedule;

    public static ArrayList<Panel> user_panels;
    public static List<Schedule> full_schedule;
    public static PanelArrayAdapter panelArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_schedule);
        //

        tv_scheduleItem_name = (TextView) findViewById(R.id.tv_panelName);
        tv_scheduleItem_time = (TextView) findViewById(R.id.tv_panelTime);
        gv_schedule = (GridView) findViewById(R.id.gv_schedule);
        //
        user_panels = new ArrayList<>();
        full_schedule = new ArrayList<>();

        // CUSTOM ARRAY ADAPTER
        panelArrayAdapter = new PanelArrayAdapter(this, Globals.userSchedulePanels);
        gv_schedule.setAdapter(panelArrayAdapter);
        gv_schedule.setClickable(true);
        gv_schedule.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Panel panel = panels.get(position);
                //FIXME: this method of getting the wanted presenter is unreliable if any presenters change position in the array
                Intent intent = new Intent(getApplicationContext(), PanelPageActivity.class);
                intent.putExtra("panel", panel);
                startActivity(intent);
            }
        });
        Globals.userSchedulePanels = new ArrayList<>();
        InitData.initUserSchedule(getApplicationContext());
    }

    //ARRAY ADAPTER
    public class PanelArrayAdapter extends ArrayAdapter<Panel> {
        private final static String TAG = "PanelArrayAdapter";
        private Context context;
        private List<Panel> list;
        public PanelArrayAdapter(Context context, List<Panel> list) {
            super(context, R.layout.panel_layout, list);
            this.context = context;
            this.list = list;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // FIXME: reuse convert view if available
            View rowView = inflater.inflate(R.layout.panel_layout, parent, false);
            TextView tv_panelName = rowView.findViewById(R.id.tv_panelName);
            TextView tv_panelDescription = rowView.findViewById(R.id.tv_panelDescription);
            final ImageView iv_panelImage = rowView.findViewById(R.id.iv_panelPageImage);
            tv_panelName.setText(list.get(position).getName());
            tv_panelDescription.setText(list.get(position).getDescription());
            final Panel panel = list.get(position);

            Call<ResponseBody> call = Globals.conventionAPI.getPresenterPhoto(panel.getPresenter());
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        try {
                            ResponseBody rb = response.body();
                            byte[] blobAsBytes = rb.bytes();
                            Bitmap btm = BitmapFactory.decodeByteArray(blobAsBytes, 0, blobAsBytes.length);
                            iv_panelImage.setImageBitmap(btm);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e(TAG, "Error fetching Image 1");
                        Toast.makeText(getApplicationContext(), "Error fetching Image 1", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e(TAG, "Error fetching Image 2", t);
                    Toast.makeText(getApplicationContext(), "Error fetching Image 2", Toast.LENGTH_SHORT).show();

                }
            });
            return rowView;
        }
    }

}
