//
//  PresenterCell.swift
//  Conferance
//
//  Created by Sharra on 2019-04-05.
//  Copyright © 2019 Sharra. All rights reserved.
//

import UIKit

class PresenterCell: UITableViewCell {


    @IBOutlet weak var ivImage: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    
    func update(with presenter: Presenter) -> Void {
        ivImage.image = UIImage(named: presenter.picture)
        lbName.text=presenter.name
        //  imageIV.image = UIImage(named: panel.image)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
