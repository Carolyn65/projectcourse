package sharra.work;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;



@ManagedBean
@ViewScoped
public class Panel {
	int id;
	String name;
	Presenter presenter;
	String description;
	Room room;
	LocalDateTime date;
	int panelRating;
	int presenterId; 
	int roomId;
	String presenterIdStr;
	String roomIdStr;
	String dateStr;
	
	List<String> presentersOptions;
	List<String> roomsOptions;

	List<Presenter> presenters;
	List<Room> rooms;
	
	private Controller controller;
	private Logger logger = Logger.getLogger(getClass().getName());
	
	public Panel() {
		try {
		getPresenterOptions();
		getRoomOptions();
		}
		catch(Exception exc) {
			logger.info("Error fetching presenteroptions & room options " + exc);
		}
	}


	public Panel(int id, String name, Presenter presenter, String description, Room room, LocalDateTime date, int panelRating) {
		this.id = id;
		this.name = name;
		this.presenter = presenter; 
		this.description = description;
		this.room = room;
		this.date = date;
		this.dateStr=date.toString().replace("T", " ");
		this.panelRating = panelRating;
	}
	
	//get the presenter list array for populating. 
	public void getPresenterOptions() throws Exception {
		controller = Controller.getInstance();
		controller.loadPresenters();
		presentersOptions = new ArrayList<>();
		presenters = controller.getPresenters();
		
		for(int i=0; i<presenters.size(); i++) {
			presentersOptions.add(presenters.get(i).name);
			logger.info(presenters.get(i).name);
		}
		}
	
	public void getRoomOptions() throws Exception {
		controller = Controller.getInstance();
		controller.loadRooms();
		roomsOptions = new ArrayList<>();
		rooms = controller.getRooms();
		
		for(int i=0; i<rooms.size(); i++) {
			roomsOptions.add(rooms.get(i).description);
			logger.info(rooms.get(i).description);
		}
		}
	

	
	//get the id of the presenter for assignment
	public int getPresenterId(String presenterName) {
		logger.info("getpresenterID called");
		int presenterId = -1;
		
		while (presenterId == -1) {
			
			for (int i=0; i<presenters.size(); i++) {
				if (presenterName == presenters.get(i).name) {
					presenterId = presenters.get(i).id;
				}
			}
			break;
		}
		//if id is not found for some weird reason set to default presenter. 
		
		if (presenterId == -1) {
			presenterId=3;
		}
		return presenterId; 
	}
	
	public int getRoomId(String roomDesc) {
		logger.info("getroomrID called");
		int roomIdTemp = -1;
		
		while (roomIdTemp == -1) {
			
			for (int i=0; i<rooms.size(); i++) {
				if (roomDesc == rooms.get(i).description) {
					roomIdTemp = rooms.get(i).id;
				}
			}
			break;
		}
		//if id is not found for some weird reason set to default presenter. 
		
		if (roomIdTemp == -1) {
			roomIdTemp=2;
		}
		return roomIdTemp; 
	}


	public int getPresenterId() {

		return presenterId;
	}
	
	public String getPresenterIdStr() {
		return presenterIdStr;
	}

	public void setPresenterId(int presenterId) {
		logger.info("setPresenterId");
		this.presenterId = presenterId;
	}


	public void setPresenterIdStr(String presenterStr) {
		logger.info("SetpresenterSTR called");
		this.presenterIdStr = presenterStr;
		presenterId=getPresenterId(presenterStr);
	}
	public int getRoomId() {
		return roomId;
	}
	public String getRoomIdStr() {
		return roomIdStr;
	}

	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}


	public void setRoomIdStr(String roomIdtemp) {
		logger.info("SetRoomIdStr Called");
		this.roomIdStr = roomIdtemp;
		roomId=getRoomId(roomIdtemp);
	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Presenter getPresenter() {
		return presenter;
	}

	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Room getRoom() {
		return room;
	}


	public void setRoom(Room room) {
		this.room = room;
	}


	public LocalDateTime getDate() {
		return date;
	}
	

	//Add Code to set Date Time object with the T from a string
	public void setDate(LocalDateTime date) {	
		this.date = date;
	}


	public String getDateStr() {
		return dateStr;
	}


	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
		String str = dateStr.replace(" ", "T");
		
	}


	public int getPanelRating() {
		return panelRating;
	}


	public void setPanelRating(int panelRating) {
		this.panelRating = panelRating;
	}


	public List<String> getPresentersOptions() {
		return presentersOptions;
	}


	public void setPresentersOptions(List<String> presentersOptions) {
		this.presentersOptions = presentersOptions;
	}


	public List<String> getRoomsOptions() {
		return roomsOptions;
	}


	public void setRoomsOptions(List<String> roomsOptions) {
		this.roomsOptions = roomsOptions;
	}


	public List<Presenter> getPresenters() {
		return presenters;
	}


	public void setPresenters(List<Presenter> presenters) {
		this.presenters = presenters;
	}


	public List<Room> getRooms() {
		return rooms;
	}


	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}


	@Override
	public String toString() {
		return "Panel [id=" + id + ", name=" + name + ", presenter=" + presenter + ", description=" + description
				+ ", room=" + room + ", date=" + date + ", panelRating=" + panelRating + ", presenterId=" + presenterId
				+ ", roomId=" + roomId + ", presenterIdStr=" + presenterIdStr + ", roomIdStr=" + roomIdStr+ "]";
	}

	
	

}
