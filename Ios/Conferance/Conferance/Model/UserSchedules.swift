//
//  UserSchedules.swift
//  Conferance
//
//  Created by Sharra on 2019-04-10.
//  Copyright © 2019 Sharra. All rights reserved.
//

import Foundation

struct UserSchedules: Codable{
    //Created to store multiple users schedules on the same device.
    //file encoder would not allow nested dictionaries as a variable to be written requires them as a variale in a struct. As apparently nesting them stops them from being hashable
    
    var userSchedule = [Int:Panel]()
    
}
