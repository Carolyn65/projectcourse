package hellobobby.example.com.android_project.services;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.BitSet;

import hellobobby.example.com.android_project.entities.Panel;
import hellobobby.example.com.android_project.entities.User;

public class Globals {
    public static String BASE_URL = "http://conventionapp.sharra.work/convapi.php/";
    public static ConventionAPI conventionAPI;
    public static User user;
    public static ArrayList<Panel> userSchedulePanels = new ArrayList<>();
    public static ArrayList<Bitmap> bitmaps = new ArrayList<>();
}
