//
//  ScheduleVC.swift
//  Conferance
//
//  Created by Sharra on 2019-04-05.
//  Copyright © 2019 Sharra. All rights reserved.
//

import UIKit

class ScheduleVC: UITableViewController {
    
 
    
    
    @IBOutlet weak var filterSegControl: UISegmentedControl!
    //filter options for the segmented control
    let dayFilterOptions = ["All", "Fri", "Sat", "Sun"]
    //empty arrays for the filtering based off selected segment
    var panels = [Panel]()
    var allPan = [Panel]()
    var friPan = [Panel]()
    var satPan = [Panel]()
    var sunPan = [Panel]()
    
    
    //Function to detect filter changed and set data set for table accordingly
    @IBAction func indexChanged(_ sender: Any) {
        
        let filter = dayFilterOptions[filterSegControl.selectedSegmentIndex]
        switch filter{
        case "All":
            panels = allPan
            self.tableView.reloadData()
        case "Fri":
            panels = friPan
            self.tableView.reloadData()
        case "Sat":
            panels = satPan
            self.tableView.reloadData()
            
        case "Sun":
            panels = sunPan
            self.tableView.reloadData()
        default:
            print("Error loading data")
    }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //Setup clean arrays for coming back to this view
        panels = [Panel]()
        allPan = [Panel]()
        friPan = [Panel]()
        satPan = [Panel]()
        sunPan = [Panel]()
        //set the selected segment default for this view
        filterSegControl.selectedSegmentIndex=0
        //setup array from dictionary for tableview
        panels = Array(glPanels.values.map{$0})
        panels = panels.sorted(by: { $0.dateTime < $1.dateTime })
        //add panels to various arrays to account for filtering
        for panel in panels{
            if panel.dateTime.contains("2019-04-12"){
                friPan.append(panel)
            }
            else if panel.dateTime.contains("2019-04-13"){
                satPan.append(panel)
            }
            else if panel.dateTime.contains("2019-04-14"){
                sunPan.append(panel)
            }
        }
        allPan=panels
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //Code to save panels to the user schedule list

    }
    
    @IBAction func favAdd(_ sender: UIButton) {
        //get position of button clicked
        let buttonPostion = sender.convert(sender.bounds.origin, to: tableView)
        //get indexpath of the position clicked
        if let indexPath = tableView.indexPathForRow(at: buttonPostion) {
            print("entered code")
            let rowIndex =  indexPath.row
            let section = indexPath.section
            print(section)
            var panel = panels[rowIndex]
            if filterSegControl.selectedSegmentIndex==0{
                switch section{
                case 0:
                    panel = friPan[rowIndex]
                case 1:
                    panel = satPan[rowIndex]
                default:
                    panel = sunPan[rowIndex]
                }
            }
            
            print(panel)
            if glUserPanels[panel.id] != nil{
                
                print("removing from schedule")
                glUserPanels.removeValue(forKey: panel.id)
                glUserSchedules[gLuserObj.email]?.userSchedule=glUserPanels
            }
            else{
                print("adding to schedule")
                glUserPanels[panel.id] = panel
                glUserSchedules[gLuserObj.email]?.userSchedule=glUserPanels
            }
        }
        //Datachanged call save changes
        let dataLoad = Data()
        dataLoad.saveData()
        tableView.reloadData()
        
    }
     //returns appropriate headers based on selected control and number of sections
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        var temp = ""
        
        if filterSegControl.selectedSegmentIndex==0{
            switch section{
            case 0:
                temp = "Friday"
            case 1:
                temp = "Saturday"
            default:
                temp = "Sunday"
            }
        }
        if filterSegControl.selectedSegmentIndex==1{
            temp = "Friday"
        }
        if filterSegControl.selectedSegmentIndex==2{
           temp =  "Saturday"
        }
        if filterSegControl.selectedSegmentIndex==3{
            temp = "Sunday"
        }
        return temp
    }
    //Set section header details programatically
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor(hue: 0.5972, saturation: 1, brightness: 0.83, alpha: 1.0)
        
        let headerLabel = UILabel(frame: CGRect(x: 40, y: 8, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        headerLabel.font = UIFont(name: "Verdana", size: 20)
        headerLabel.textColor = UIColor.white
        headerLabel.text = self.tableView(self.tableView, titleForHeaderInSection: section)
        headerLabel.textAlignment = NSTextAlignment.center
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        
        return headerView
    }
    //set header height
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    
    //returns appropriate number os sections based on segment selected
    override func numberOfSections(in tableView: UITableView) -> Int {
        if filterSegControl.selectedSegmentIndex==0{
        return 3
        }
        else{
            return 1
        }
    }
 //returns appropriate size for data set based on selected segment and section
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filterSegControl.selectedSegmentIndex == 0{
        if section == 0 {
            return friPan.count
        }
        if section == 1{
                return satPan.count
            }
     if section == 2{
               return sunPan.count
           }
            
        }
        return panels.count
    }
    //sets cells based on segment and section selected.
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        

        //create template for items Deque cell
        if filterSegControl.selectedSegmentIndex == 0{
            switch indexPath.section{
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PanelCell", for: indexPath) as! ScheduleCells
                let panel = friPan[indexPath.row]
                cell.update(with: panel)
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PanelCell", for: indexPath) as! ScheduleCells
                let panel = satPan[indexPath.row]
                cell.update(with: panel)
                return cell
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PanelCell", for: indexPath) as! ScheduleCells
                let panel = sunPan[indexPath.row]
                cell.update(with: panel)
                return cell
            default:
                print("Out of range")
            }
        }
            let cell = tableView.dequeueReusableCell(withIdentifier: "PanelCell", for: indexPath) as! ScheduleCells
            let panel = panels[indexPath.row]
            cell.update(with: panel)
        
        
         return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "logOff" {
            gLuserObj=User(id: "0", name: "", email: "")
            print(gLuserObj)
        }
        if segue.identifier == "panelDetail" {
            let indexPath = tableView.indexPathForSelectedRow!
            let panel = panels[indexPath.row]
            
            let navVC = segue.destination as! UINavigationController
            let detailViewController = navVC.viewControllers.first as! PanelDetailsVC
            detailViewController.panel = panel
        }
    }
    
    @IBAction func unwindToSchedule(_ unwindSegue: UIStoryboardSegue) {
        
        switch unwindSegue.identifier {
        case "fromPanel":
            _ = unwindSegue.source as! PanelDetailsVC
            
        default:
            print("Went to default")
        }
    }
    

  
}
