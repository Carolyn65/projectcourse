package hellobobby.example.com.android_project.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import hellobobby.example.com.android_project.entities.User;
import hellobobby.example.com.android_project.R;
import hellobobby.example.com.android_project.services.Globals;
import hellobobby.example.com.android_project.services.ConventionAPI;
import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainLoginActivity extends AppCompatActivity {

    EditText et_email;
    EditText et_password;
    Button bt_login;
    TextView tv_useless;
    TextView tv_here;
    Button bt_facebookLogin;
    public static final String EXTRA_ROOM="";
    public SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_login);
        //
        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);
        bt_login = (Button) findViewById(R.id.bt_login);
        bt_facebookLogin = (Button) findViewById(R.id.bt_facebookLogin);
        tv_useless = (TextView) findViewById(R.id.tv_useless);
        tv_here = (TextView) findViewById(R.id.tv_here);
        //
        tv_here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CreateAccountActivity.class);
                startActivity(intent);
            }
        });

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        startRetrofit();
    }

    public void startRetrofit() {
        Gson gson = new GsonBuilder().setLenient().create();
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request originalRequest = chain.request();
                Request.Builder builder = originalRequest.newBuilder().header("X-Authorization",
                        Credentials.basic(sharedPreferences.getString("username", ""), sharedPreferences.getString("password", "")));
                Request newRequest = builder.build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Globals.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();
        Globals.conventionAPI = retrofit.create(ConventionAPI.class);
    }


    public void onLoginClick(View v) {
        String email = et_email.getText().toString();
        String password = et_password.getText().toString();
        sharedPreferences.edit().putString("username", email).putString("password", password).commit();
//        Globals.login_email = sharedPreferences.getString("username", "");
//        Globals.login_password = sharedPreferences.getString("password", "");
        startRetrofit();

        Call<User> call = Globals.conventionAPI.getAuthUser(email);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User user = response.body();
                Globals.user = user;
                et_password.setText("");
                if (user != null) {
                    Intent intent = new Intent(getApplicationContext(), PanelListActivity.class);
                    intent.putExtra("user", user);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Null Pointer on return", Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetching user failed (2)", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void onFacebookClick(View v) {
        //TODO: Implement facebook integration here
    }
    //TODO: Logout
    //TODO: List of rooms
}
