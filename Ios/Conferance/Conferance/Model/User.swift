//
//  User.swift
//  Conferance
//
//  Created by Sharra on 2019-04-05.
//  Copyright © 2019 Sharra. All rights reserved.
//

import Foundation

struct Users:Codable{
   let results: [User]
}

struct User: Codable{
    var id: String?
    var name: String
    var email: String
    var password: String?

    //various inits used by database and application at various points.
    
    //Create without ID for database insertion
    init(name name2: String, email email2: String, password password2: String) {
        self.name=name2
        self.email=email2
        self.password=password2
    }
    
   // create with id and password for local use
    init(id: String, name name2: String, email email2: String, password password2: String) {
        self.id=id
        self.name=name2
        self.email=email2
        self.password=password2
    }
    //get from database insertion without password for security 
    init(id id2: String, name name2: String, email email2: String) {
        self.id=id2
        self.name=name2
        self.email=email2
    }

    
}
