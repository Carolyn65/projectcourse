//
//  Data.swift
//  Conferance
//
//  Created by Sharra on 2019-04-09.
//  Copyright © 2019 Sharra. All rights reserved.
//

import Foundation

struct Data{
     // Add data for if file doesn't exist while API is offline
    func addRooms(){
    
    let room1 = Room(id: 1, description: "220a - the cool room", gpsLoc: 1)
    let room2 = Room(id: 2, description:"510 - The very not cool room", gpsLoc:2)
    let room3 = Room(id: 3, description: "220b - The semi-Cool room", gpsLoc: 3)
    
        glRooms[room1.id] = room1
        glRooms[room2.id] = room2
        glRooms[room3.id] = room3
    }
    
    func addPresenters(){
        let pres1 = Presenter(id: 1, name: "The Brain", bio: "The brain is a genius megalomaniacle mouse whose soul purpose is to take over the world. He rarely is seen without his partner Pinky in tow. He is here to detail his latest schemes for your amusement!", picture: "brain")
        let pres2 = Presenter(id: 2, name: "Muppets", bio: "These singers have been an online sensation covering such hits as popcorn & bohemian rhapsody in a comical manner", picture: "muppets")
        let pres3 = Presenter(id: 3, name: "Ricky", bio: "Ricky is an astronaut/cat extrordinaire. Born in 1702, Ricky is one of the most known astronauts from the 18th century. He died at in 2004, at the age of 302. He was then raised from the dead for this presentation.", picture: "ricky")
        let pres4 = Presenter(id: 4, name: "Nine Inch Nails", bio: "This musical rock experiment has been around since the 80's always trying new things. Typically industrial music", picture: "nin")

        glPresenters[pres1.id] = pres1
        glPresenters[pres2.id] = pres2
        glPresenters[pres3.id] = pres3
        glPresenters[pres4.id] = pres4
    }
    
    func addPanels(){
       
        let panel1 = Panel(id: 1, name: "Comedy in music", presenter: glPresenters[2]!, description: "his panel will explore the various ways of including comedy in your musical stylings", room: glRooms[2]!, dateTime: "2019-04-14 12:00", time: "12:00", panelRating: 0)
        let panel2 = Panel(id: 2, name: "Rickys Plan to Save the World", presenter: glPresenters[3]!, description: "Saving the world is not an easy task, for Ricky has a fool proof plan", room: glRooms[1]!, dateTime: "2019-04-14 16:00", time: "16:00", panelRating: 0)
        let panel3 = Panel(id: 3, name: "Crazy Schemes to take over the world", presenter: glPresenters[1]!, description: "In this panel The Brain will tell anticdotes of trying to take over the world and take audience questions,", room: glRooms[3]!, dateTime: "2019-04-13 14:00", time: "14:00", panelRating: 0)
        let panel4 = Panel(id: 4, name: "The testing Panel", presenter: glPresenters[4]!, description: "The silly testing panel where everything blows up,", room: glRooms[2]!, dateTime: "2019-04-13 14:00", time: "14:00", panelRating: 0)
        let panel5 = Panel(id: 5, name: "Bobs Your uncle", presenter: glPresenters[3]!, description: "Bob really is your uncle ,", room: glRooms[1]!, dateTime: "2019-04-12 10:00", time: "10:00", panelRating: 0)
        let panel6 = Panel(id: 6, name: "Revenge of the testing Panel", presenter: glPresenters[1]!, description: "The silly testing panel where everything blows up Yet again", room: glRooms[2]!, dateTime: "2019-04-13 11:00", time: "11:00", panelRating: 0)
        let panel7 = Panel(id: 7, name: "The super awesome Cool guys panel", presenter: glPresenters[1]!, description: "The silly testing panel where everything blows up,", room: glRooms[2]!, dateTime: "2019-04-13 14:00", time: "14:00", panelRating: 0)
        let panel8 = Panel(id: 8, name: "More test data with brain", presenter: glPresenters[1]!, description: "Yay test data! ", room: glRooms[2]!, dateTime: "2019-04-13 15:00", time: "15:00", panelRating: 0)
        let panel9 = Panel(id: 9, name: "more test data with brain 2", presenter: glPresenters[1]!, description: "Yay test data 3!", room: glRooms[2]!, dateTime: "2019-04-13 17:00", time: "17:00", panelRating: 0)
        let panel10 = Panel(id: 10, name: "The testing Panel", presenter: glPresenters[4]!, description: "The silly testing panel where everything blows up,", room: glRooms[2]!, dateTime: "2019-04-13 14:00", time: "14:00", panelRating: 0)
        let panel11 = Panel(id: 11, name: "How to Learn swift in 2 weeks", presenter: glPresenters[2]!, description: "It's crazy it's impossible it's going to happen!", room: glRooms[2]!, dateTime: "2019-04-13 12:00", time: "12:00", panelRating: 0)
        let panel12 = Panel(id: 12, name: "How to Clear traffic in montreal??", presenter: glPresenters[3]!, description: "I'll give you a hint it involves a plow and a willingness to go to jail!", room: glRooms[2]!, dateTime: "2019-04-14 10:00", time: "10:00", panelRating: 0)
        let panel13 = Panel(id: 13, name: "Potholes...", presenter: glPresenters[3]!, description: "They can eat a car, they can swallow a building but what is the government really doing to fix this menace?", room: glRooms[2]!, dateTime: "2019-04-13 15:00", time: "15:00", panelRating: 0)
        
        glPanels[panel1.id] = panel1
        glPanels[panel2.id] = panel2
        glPanels[panel3.id] = panel3
        glPanels[panel4.id] = panel4
        glPanels[panel5.id] = panel5
        glPanels[panel6.id] = panel6
        glPanels[panel7.id] = panel7
        glPanels[panel8.id] = panel8
        glPanels[panel9.id] = panel9
        glPanels[panel10.id] = panel10
        glPanels[panel11.id] = panel11
        glPanels[panel12.id] = panel12
        glPanels[panel13.id] = panel13
        
    }
    
    func addUsers(){
        let user1 = User(id: "1", name: "bob", email: "666", password: "666")
        let user2 = User(id: "2", name: "Sharra", email: "sharra@shaw.ca", password: "Abcd1234")
        glUsers[user1.email]=user1
        glUsers[user2.email]=user2
    }
    
    func addUserSchedule(){
        let sched1 = [1 : glPanels[1], 4: glPanels[4]]
        let sched2 = [2 : glPanels[2], 1: glPanels[1], 3: glPanels[3] ]
       
        
        glUserSchedules["666"] = UserSchedules(userSchedule: sched1 as! [Int : Panel])
        glUserSchedules["sharra@shaw.ca"] = UserSchedules(userSchedule: sched2 as! [Int : Panel])
        
    }
    
    //Save to file method
    //calls method from Storage class
    func saveData()  {
        print("Saving Data")
        Storage.store(glPanels, to: .documents, as: "glPanels")
        Storage.store(glUserSchedules, to: .documents, as: "glUserSchedules")
        Storage.store(glUsers, to: .documents, as: "glUsers")
        Storage.store(glRooms, to: .documents, as: "glRooms")
        Storage.store(glPresenters, to: .documents, as: "glPresenters")

    }
    
    //Get path for file
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    //get data from files if file doesn't exist run above add method.
    //calls method from storage class
    func readFile(){
        print("Reading data")
        if Storage.fileExists("glRooms", in: .documents){
        glRooms = Storage.retrieve("glRooms", from: .documents, as: [Int:Room].self)
        }
        else{
            print("file doesn't exist")
            addRooms()
        }
        if Storage.fileExists("glPresenters", in: .documents){
        glPresenters = Storage.retrieve("glPresenters", from: .documents, as: [Int:Presenter].self)
        }
        else{
            print("presenters file doesn't exist")
            addPresenters()
        }
        
        if Storage.fileExists("glPanels", in: .documents){
        glPanels = Storage.retrieve("glPanels", from: .documents, as: [Int:Panel].self)
        }
        else{
            print("panels doesn't exist")
            addPanels()
        }

        if Storage.fileExists("glUsers", in: .documents){
        glUsers = Storage.retrieve("glUsers", from: .documents, as: [String:User].self)
        }
        else{
            //Note: this should be changed to just let them register.
            print("users file doesn't exist")
            addUsers()
        }
        
        if Storage.fileExists("glUserSchedules", in: .documents){
        glUserSchedules = Storage.retrieve("glUserSchedules", from: .documents, as: [String:UserSchedules].self)
            print("read user schedule")
        }
        else{
            print("userSchedules doesn't exist")
            addUserSchedule()
        }

    }

    
    
}
