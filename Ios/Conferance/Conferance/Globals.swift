//
//  Globals.swift
//  Conferance
//
//  Created by Sharra on 2019-04-05.
//  Copyright © 2019 Sharra. All rights reserved.
//

import Foundation

//define global arrays for Presenters, Panels, Rooms, Panels by Day (Fri, Sat, Sun)
//Call DB API from here to get data.

var gLuserObj = User(id: "0", name: "", email: "") //Current logged in user
var glUsers = [String:User]() //Store by user email: User data
var glPanels = [Int:Panel]() //store by Id: Panel
var glPresenters = [Int:Presenter] ()//Store by presenter ID: Presenter
var glRooms = [Int:Room]() // store by room id: Room info
var glUserSchedules = [String:UserSchedules]() // String user email - Stored item User schedule

var glUserPanels = [Int:Panel]() //Store by ID: panel //generated during runtime
var presenterPan = [Panel]()//generated during runtime



//in use only with the api which is not implemented currently
var gLapiPath = "https://conventionapp.sharra.work/convapi.php/"
var gLbase64LoginString = "dummy".data(using: String.Encoding.utf8)!.base64EncodedString()

