package hellobobby.example.com.android_project.services;

import java.util.List;

import hellobobby.example.com.android_project.entities.Panel;
import hellobobby.example.com.android_project.entities.Presenter;
import hellobobby.example.com.android_project.entities.Room;
import hellobobby.example.com.android_project.entities.Schedule;
import hellobobby.example.com.android_project.entities.User;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ConventionAPI {

    @GET("panels")
    Call<List<Panel>> getPanels();

    @GET("presenters/{id}")
    Call<Presenter> getPresenterById(@Path("id") int id);

    @GET("rooms/{id}")
    Call<Room> getRoomById(@Path("id")int id);

    @POST("users")
    Call<Integer> addUser(@Body User user);

    @GET("users")
    Call<List<User>> getAllUsers();

    @GET("users/{email}")
    Call<User> getAuthUser(@Path("email") String email);

    @POST("schedule/personal")
    Call<Schedule> setPeronalSchedule(@Body Schedule schedule);

    @GET("schedule/personal/{id}")
    Call<List<Schedule>> getPersonalSchedule(@Path("id") int id);

    @DELETE("schedule/{id}")
    Call<Schedule> deleteSchedule(@Path("id")int id);

    //-----------OLD--------------//
//    @GET("register.php")
//    Call<User> performUserRegistration(@Query("name") String name, @Query("email") String username, @Query("password") String password);
//
//    @GET("login.php")
//    Call<User> performUserLogin(@Query("email") String username, @Query("password") String password);

    @GET("presenters/{id}/picture")
    Call<ResponseBody> getPresenterPhoto(@Path("id")int id);

}
