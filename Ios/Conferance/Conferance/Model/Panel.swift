//
//  Panels.swift
//  Conferance
//
//  Created by Sharra on 2019-04-05.
//  Copyright © 2019 Sharra. All rights reserved.
//

import Foundation

struct Panel: Codable{
    var id: Int
    var name: String
    var presenter: Presenter
    var description: String
    var room: Room
    var dateTime: String
    var time: String
    var panelRating: Int
    

}
