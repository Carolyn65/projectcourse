//
//  PersonalScheduleCell.swift
//  Conferance
//
//  Created by Sharra on 2019-04-09.
//  Copyright © 2019 Sharra. All rights reserved.
//

import UIKit

class PersonalScheduleCell: UITableViewCell {

    @IBOutlet weak var ivImage: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbDesc: UILabel!
    @IBOutlet weak var lbTime: UILabel!
    @IBOutlet weak var starBtn: UIButton!
    
    func update(with panel: Panel) -> Void {
        ivImage.image = UIImage(named: panel.presenter.picture)
        lbTitle.text = panel.name
        lbDesc.text = panel.description
         lbTime.text  = panel.time
        //set image for favourite button based on if in schedule already or not
        if glUserPanels[panel.id] != nil{
            starBtn.setBackgroundImage(UIImage(named: "starfilled"), for: UIControl.State.normal)
        }
        else{
            starBtn.setBackgroundImage(UIImage(named: "starEmpty"), for: UIControl.State.normal)
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
