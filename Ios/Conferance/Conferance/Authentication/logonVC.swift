//
//  logonVC.swift
//  Conferance
//
//  Created by Sharra on 2019-04-05.
//  Copyright © 2019 Sharra. All rights reserved.
//

import UIKit

class logonVC: UIViewController {

    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userPassword: UITextField!
  
    
    @IBOutlet weak var lbError: UILabel!
    var userEmail = "user"
    var pwd = "pass"
    let ItemsController1 = ItemsController()
    var userIDReg = ""
    
    
    
    
    // create the request
    

    @IBAction func Submit(_ sender: Any) {
        //trigger logon call to api and handle answer
        //Should Segue? Success Yes, Fail No and display error
        //variable to determine?
        userName.backgroundColor = UIColor.white
        userPassword.backgroundColor = UIColor.white
        
        if userName.text == "" || userPassword.text == ""{
            showToast(message:"Missing Information")
        }
        else{
           
            userEmail = userName.text!
            pwd = userPassword.text!
            
            let loginString = "\(userEmail):\(pwd)"
            guard let loginData = loginString.data(using: String.Encoding.utf8) else {
                return
            }
            gLbase64LoginString = loginData.base64EncodedString()
            
            let success = self.ItemsController1.fetchUser(email: self.userEmail, pwd: self.pwd)
            
            switch success{
            case "Success":
                if glUserSchedules[userEmail] != nil{
                    glUserPanels=glUserSchedules[userEmail]!.userSchedule
                    
                }
                self.performSegue(withIdentifier: "logOn", sender: nil)
            case "Password incorrect":
                userPassword.backgroundColor = UIColor.red
                lbError.text="Incorrect Password"
    
            case "UID incorrect":
                userName.backgroundColor = UIColor.red
                lbError.text="Incorrect User Email"

                
            default:
                lbError.text="Failed Logon"
            }
            
            //when api is back up and working
//            self.ItemsController1.fetchUser(email: self.userEmail){(completion: User?) in
//                DispatchQueue.main.async{
//                    //update your UI stuff here.
//
//                if(gLuserObj.email==self.userEmail){
//                    self.performSegue(withIdentifier: "logOn", sender: nil)
//                }
//                else{
//                    print(self.userEmail)
//                    print(gLuserObj)
//                    print("Global object")
//                    //msg gets shown but print doesn't happen.
//                    self.showToast(message: "Invalid UserName or Password")
//                }
//              }
//            }
            
        }
        
        // enter code to cause this to wait on response from other
      
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        userName.text = userIDReg
  
    }
    override func viewDidAppear(_ animated: Bool) {
        let dataLoad = Data()
        dataLoad.readFile()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Will only happen once here to have initial data set for next screen.
        //Add method to check for local File If exists Load from there instead.

        
//        dataLoad.addRooms()
//        dataLoad.addPresenters()
//        dataLoad.addPanels()
//        dataLoad.addUsers()
//        dataLoad.addUserSchedule()
        //Add method to load from file in data

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "logOn" {
                
                print("segue main schedule")
            }
        
        else if segue.identifier == "register"{
                print("segue Registration")
    
        }
    }
    
    
    
    @IBAction func unwindToFirstVC(_ unwindSegue: UIStoryboardSegue) {
        
        let dataLoad = Data()
        dataLoad.saveData()
        
//        switch unwindSegue.identifier {
//        case "fromRegister":
//            let vc = unwindSegue.source as! RegistrationVC
//
////        case "fromChangePW":
////            let vc = unwindSegue.source as! Change_password
////            users = vc.users
//
//        default:
//            print("Went to default")
//        }
    }
    
}


        
extension logonVC {
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    } }

