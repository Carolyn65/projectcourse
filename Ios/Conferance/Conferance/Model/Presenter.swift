//
//  Presenter.swift
//  Conferance
//
//  Created by Sharra on 2019-04-05.
//  Copyright © 2019 Sharra. All rights reserved.
//

import Foundation

struct Presenter: Codable{
    var id: Int
    var name: String
    var bio: String
    var picture: String
}
