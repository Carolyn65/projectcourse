package hellobobby.example.com.android_project.entities;

import java.io.Serializable;

public class Room implements Serializable {

    private int id;
    private String description;
    private int gpsLoc;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getGpsLoc() {
        return gpsLoc;
    }

    public void setGpsLoc(int gpsLoc) {
        this.gpsLoc = gpsLoc;
    }
}
