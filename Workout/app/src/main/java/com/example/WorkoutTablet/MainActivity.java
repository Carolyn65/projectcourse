package com.example.WorkoutTablet;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;

public class MainActivity extends AppCompatActivity implements WorkoutListFragment.Listener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

  @Override
    public void itemClicked(long id){
        //get referance to fragment container
        View fragmentContainer = findViewById(R.id.fragment_container);
        //Use fragment containers presence to determine layout sie.
        if (fragmentContainer != null){
            //add fragment to the frame layout

            WorkoutDetailFragment details = new WorkoutDetailFragment();
            //Start fragment transaction
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            details.setWorkout(id);
            //replace the fragment
            ft.replace(R.id.fragment_container, details);
            //get the new and old fragments to fade in and out
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            //add the transaction to the back stack
            ft.addToBackStack(null);
            //Commit the transaction
            ft.commit();

        }
        else {
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra(DetailActivity.EXTRA_WORKOUT_ID, (int) id);
            startActivity(intent);
        }
  }
}
