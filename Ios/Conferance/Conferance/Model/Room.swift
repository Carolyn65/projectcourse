//
//  Rooms.swift
//  Conferance
//
//  Created by Sharra on 2019-04-05.
//  Copyright © 2019 Sharra. All rights reserved.
//

import Foundation
struct Room: Codable{
    var id: Int
    var description: String
    var gpsLoc: Int
    
}
