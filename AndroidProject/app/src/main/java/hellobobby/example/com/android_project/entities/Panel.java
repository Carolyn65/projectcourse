package hellobobby.example.com.android_project.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class Panel implements Serializable {
    @SerializedName("id") //this doesn't need serializedName as the variables are named the same in json and in the class
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("presenter")
    private int presenter;
    @SerializedName("description")
    private String description;
    @SerializedName("room")
    private int room;

//    //inserted to try option
//    @SerializedName("roomDescription")
//    private String roomDescription;

    @SerializedName("dateTime")
    private Date dateTime;
    @SerializedName("panelRating")
    private int panelRating;

    private Presenter presenterObject;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPresenter() {
        return presenter;
    }

    public void setPresenter(int presenter) {
        this.presenter = presenter;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRoom() {
        return room;
    }

    public void setRoom(int room) {
        this.room = room;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public int getPanelRating() {
        return panelRating;
    }

    public void setPanelRating(int panelRating) {
        this.panelRating = panelRating;
    }

    public Presenter getPresenterObject() {
        return presenterObject;
    }

    public void setPresenterObject(Presenter presenterObject) {
        this.presenterObject = presenterObject;
    }
////inserted to try option
//    public String getRoomDescription() {
//        return roomDescription;
//    }
//
//    public void setRoomdescription(String roomdescription) {
//        this.roomDescription = roomdescription;
//    }

    @Override
    public String toString() {
        return "Panel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", presenter=" + presenter +
                ", description='" + description + '\'' +
                ", room=" + room +
                ", dateTime=" + dateTime +
                ", panelRating=" + panelRating +
                '}';
    }
}
