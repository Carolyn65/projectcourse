package sharra.work;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.imageio.ImageIO;
import javax.servlet.http.Part;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;



@ManagedBean
@ViewScoped
public class Presenter {
	int id;
	String name;
	Blob picture;
	byte[] pictureArr;
	String bio;
	String imageString;
	Part file;
	private Logger logger = Logger.getLogger(getClass().getName());
	
	
	
	public Presenter(){}

	public Presenter(int id, String name, Blob picture, byte[] pictureArr, String bio) throws Exception {
		super();
		this.id = id;
		this.name = name;
		this.pictureArr=pictureArr;
		this.picture = picture; 
		this.bio = bio;
		
		try {
			imageString= new String(Base64.getEncoder().encodeToString(pictureArr));
			
		} catch (Exception e) {
		    logger.info("Error converting picture");
		}
		
	}
	
	public void validate(FacesContext context, UIComponent  component, Object value) {
		logger.info("validate method called");
		Part file = (Part) value;
		if(file.getSize() > 12000000) {
			throw new ValidatorException(new FacesMessage("File is too large"));
		}
		if(!file.getContentType().endsWith("jpg")) {
		}
		else throw new ValidatorException(new FacesMessage("File is not a jpg file"));
		
	}
	
	
	public String addPresenter(Presenter thePresenter) {
		try {
		Presenter thePresenterUpdated = upload(thePresenter);
		 Controller controller = Controller.getInstance();
         controller.addPresenter(thePresenterUpdated);
		
		}
		catch(Exception exc) {
			// send this to server logs
			logger.info("add presenter failed");
			logger.log(Level.SEVERE, "Error updating presenter: " + thePresenter, exc);
			return "presenters_list?faces-redirect=true";
		}
		
		return "presenters_list?faces-redirect=true";
	}
	
	public String updatePresenter(Presenter thePresenter) {
	
		try {
			Presenter thePresenterUpdated=upload(thePresenter);
			 Controller controller = Controller.getInstance();
	         controller.updatePresenter(thePresenterUpdated);
			
			}
			catch(Exception exc) {
				logger.info("updatePresenterFailed");
				// send this to server logs
				logger.log(Level.SEVERE, "Error updating presenter: " + thePresenter, exc);
				return "presenters_list?faces-redirect=true";
			}
		
		return "presenters_list?faces-redirect=true";
	}
	
	
	public Presenter upload(Presenter thePresenter) throws IOException, SerialException, SQLException {
	logger.info("upload method called");
	
	 try (InputStream input = file.getInputStream()) {
		 String fileName = file.getSubmittedFileName();
		 
		 File tmpDir = new File("/Users/Sharra/projectcourse/ConfWebApp/WebContent/uploads/" + fileName);
		 //update this path for online server file saving. 
		 		if (tmpDir.exists()==false) {
		 			 Files.copy(input, new File("/Users/Sharra/projectcourse/ConfWebApp/WebContent/uploads", fileName).toPath());
		 		}
		        
		         //needs updated for other os or server path
		         File file = new File("/Users/Sharra/projectcourse/ConfWebApp/WebContent/uploads", fileName);
		         
		         BufferedImage bufferimage = ImageIO.read(file);
		         ByteArrayOutputStream output = new ByteArrayOutputStream();
		         ImageIO.write(bufferimage, "jpg", output );
		         thePresenter.setPictureArr(output.toByteArray());
		         thePresenter.setImageString(new String(Base64.getEncoder().encodeToString(thePresenter.getPictureArr())));
		         thePresenter.setPicture(new SerialBlob(thePresenter.getPictureArr()));
		         input.close();
		         return thePresenter;
		         
		         }
		         
		     catch (IOException e) {
		         e.printStackTrace();
		         logger.info("error converting image 2");
		
		     }
	 return thePresenter;
	
	}
	
	

	public Part getFile() {
		return file;
	}

	public void setFile(Part file) {
		this.file = file;
	}

	public String getImageString() {
		return imageString;
	}

	public void setImageString(String imageString) {
		this.imageString = imageString;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public byte[] getPictureArr() {
		return pictureArr;
	}

	public void setPictureArr(byte[] pictureArr) {
		this.pictureArr = pictureArr;
	}

	public Blob getPicture() {
		//Call convert to byte array and picture method. 
		return picture;
	}

	public void setPicture(Blob picture) {
		//Setup second set method for Admin to convert picture to blob
		this.picture = picture;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}


	@Override
	public String toString() {
		return "Presenter [id=" + id + ", name=" + name + ", bio=" + bio + "]";
	}
	
	

}
