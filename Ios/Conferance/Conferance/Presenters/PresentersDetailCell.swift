//
//  PresentersDetailCell.swift
//  Conferance
//
//  Created by Sharra on 2019-04-09.
//  Copyright © 2019 Sharra. All rights reserved.
//

import UIKit

class PresentersDetailCell: UITableViewCell {

    
    
    @IBOutlet weak var lbName: UILabel!
    
    @IBOutlet weak var lbDate: UILabel!
    
    func update(with panel: Panel) -> Void {
        lbName.text=panel.name
        lbDate.text=panel.dateTime
        //  imageIV.image = UIImage(named: panel.image)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
