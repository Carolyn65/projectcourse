//
//  PresenterDetailsVC.swift
//  Conferance
//
//  Created by Sharra on 2019-04-05.
//  Copyright © 2019 Sharra. All rights reserved.
//

import UIKit

class PresenterDetailsVC: UITableViewController {
    
    var presenter: Presenter?

    @IBOutlet weak var ivImage: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbBio: UILabel!
    
    //outlet for nested tableview
    @IBOutlet weak var nestedTable: UITableView!
    //instance of delegate to set data for dynamic nested table
    var dataSource = DataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //set full panel array to search
        let panels = Array(glPanels.values.map{$0})
        //Search panels to find panels the presenter is on and add to presenterPan
        for panel in panels{
            if panel.presenter.id == presenter!.id{
                presenterPan.append(panel)
            }
            //Set delegate source and data for nested table view
            nestedTable.delegate=dataSource
            nestedTable.dataSource=dataSource
            
        }
    }
    
    // set data for presenter info
    override func viewWillAppear(_ animated: Bool) {
       lbName.text=presenter?.name
        lbBio.text=presenter?.bio
        ivImage.image = UIImage(named: presenter!.picture)
    }

    //segue on tap to panel details that presenter is on
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PanelDetails" {
            print("Details1")
            let indexPath = nestedTable.indexPathForSelectedRow!
            let panel = presenterPan[indexPath.row]
            
            let navVC = segue.destination as! UINavigationController
            let detailViewController = navVC.viewControllers.first as! PanelDetailsVC
            detailViewController.panel = panel
        }
  
    }
    //return to previous screen in stack
    @IBAction func backBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //clear the presenters panels list global upon leaving screen
        presenterPan = [Panel]()
    }
    
    }



//Set data source for nested dynamic TableView using separate class
class DataSource: NSObject, UITableViewDataSource, UITableViewDelegate{
    
 
    func numberOfSections(in tableView: UITableView) -> Int {
            return 1
    }
    
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return presenterPan.count
    }
    
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "PresentersDetailCell", for: indexPath) as! PresentersDetailCell
        let panel = presenterPan[indexPath.row]
        cell.update(with: panel)
        return cell
        
        
    }
    

}

