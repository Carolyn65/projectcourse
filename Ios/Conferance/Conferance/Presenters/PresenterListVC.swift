//
//  PresenterListVC.swift
//  Conferance
//
//  Created by Sharra on 2019-04-05.
//  Copyright © 2019 Sharra. All rights reserved.
//

import UIKit

class PresenterListVC: UITableViewController {

    var presenters = [Presenter]()
  
    //set presenters as array from dictionary
    override func viewWillAppear(_ animated: Bool) {
        presenters = Array(glPresenters.values.map{$0})
        self.tableView.reloadData()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return presenters.count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PresenterCell", for: indexPath) as! PresenterCell
        let presenter = presenters[indexPath.row]
        cell.update(with: presenter)
        return cell
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "logOff" {
            _ = segue.destination as! UINavigationController
            gLuserObj=User(id: "0", name: "", email: "")
            print(gLuserObj)
        }
        if segue.identifier == "presenterDetail" {
            let indexPath = tableView.indexPathForSelectedRow!
            let presenter = presenters[indexPath.row]
            
            let navVC = segue.destination as! UINavigationController
            let detailViewController = navVC.viewControllers.first as! PresenterDetailsVC
            detailViewController.presenter = presenter
            
        }
    }
    
    @IBAction func unwindToPresenterList(_ unwindSegue: UIStoryboardSegue) {
        
        switch unwindSegue.identifier {
        case "fromPresenter":
            _ = unwindSegue.source as! PresenterDetailsVC
            
        default:
            print("Went to default")
        }
    }
}
