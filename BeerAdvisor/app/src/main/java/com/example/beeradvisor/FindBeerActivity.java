package com.example.beeradvisor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;
import java.util.List;

public class FindBeerActivity extends AppCompatActivity {
    private BeerExpert expert = new BeerExpert();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_beer);
    }

    public void onClickFindBeer(View view){
        //text view reference
        TextView brands = findViewById(R.id.brands);
        //get a reference to the spinner
        Spinner color = findViewById(R.id.color);
        //get selected item in spinner
        String beerType = String.valueOf(color.getSelectedItem());
        //display the selected item
        brands.setText(beerType);
        List <String> brandsList = expert.getBrands(beerType);
        StringBuilder brandsFormatted = new StringBuilder();
        for (String brand :brandsList){
        brandsFormatted.append(brand).append('\n');
        }
        brands.setText(brandsFormatted);
    }


}
