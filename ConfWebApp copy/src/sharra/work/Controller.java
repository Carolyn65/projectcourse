package sharra.work;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

@ManagedBean
@SessionScoped
public class Controller {
	
	private static Controller instance;
	private List<Panel> panels;
	private List<Presenter> presenters;
	private List<Room> rooms;
	private List<String> presenterList;
	private DbUtil DbUtil;
	private Logger logger = Logger.getLogger(getClass().getName());

	
	
	
	// 1- Add theSearchName attribute -----------------------------------------------
	
	private String theSearchName;
	private String theSearchNamePresenters;
	// ------------------------------------------------------------------------------

	// 2- theSearchName getter and setter -------------------------------------------
	public String getTheSearchName() {
		return theSearchName;
	}

	public void setTheSearchName(String theSearchName) {
		this.theSearchName = theSearchName;
	}
	
	
	// ------------------------------------------------------------------------------

	public String getTheSearchNamePresenters() {
		return theSearchNamePresenters;
	}

	public void setTheSearchNamePresenters(String theSearchNamePresenters) {
		this.theSearchNamePresenters = theSearchNamePresenters;
	}

	public List<Panel> getPanels() {
		return panels;
	}
	public List<Presenter> getPresenters() {
		return presenters;
	}
	
	
	public List<Room> getRooms() {
		return rooms;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}

	public Controller() throws Exception {
		//students = new ArrayList<>();
		DbUtil = DbUtil.getInstance();
	}
	
	public boolean validateUsernamePassword(String user, String pwd) {
		logger.info("made it to student Controller pre validate call");
		boolean valid = DbUtil.validate(user, pwd);
		logger.info("made it to student Controller post validate call");
		if (valid) {
			return true;
		} else {
			return false;
		}
	}
//	
//	// 3- Add logic to loadStudents() method which will be called when page is reloaded
//	// ------------------------------------------------------------------------------
	public void loadPanels() {

		logger.info("Loading panels");
		logger.info("theSearchName = " + theSearchName);

		try {

			// ------------------------------------------------------------------------------
			if (theSearchName != null && theSearchName.trim().length() > 0) {
				// Search for student by name
				panels = DbUtil.searchPanels(theSearchName);
			}
			// ------------------------------------------------------------------------------

			else {
			//	 Get all panels from database
				panels = DbUtil.getPanels();
			}

		} catch (Exception exc) {
			// Send this to server logs
			logger.log(Level.SEVERE, "Error loading panels", exc);

			// Add error message for JSF page
			addErrorMessage(exc);
			
		} finally {
			// Reset the search info
			theSearchName = null;
		}
		
		
	}
	
	public void loadPresenters() {

		logger.info("Loading presenters");
		logger.info("theSearchNamePresenters = " + theSearchNamePresenters);

		try {

			// ------------------------------------------------------------------------------
			if (theSearchNamePresenters != null && theSearchNamePresenters.trim().length() > 0) {
				// Search for student by name
				presenters = DbUtil.searchPresenters(theSearchNamePresenters);
			}
			// ------------------------------------------------------------------------------

			else {
			//	 Get all panels from database
				presenters = DbUtil.getPresenters();
			}

		} catch (Exception exc) {
			// Send this to server logs
			logger.log(Level.SEVERE, "Error loading presenters", exc);

			// Add error message for JSF page
			addErrorMessage(exc);
			
		} finally {
			// Reset the search info
			theSearchNamePresenters = null;
		}
		
		
	}
	
	public void loadRooms() {

		logger.info("Loading rooms");
		try {	
			//	 Get all rooms from database
				rooms = DbUtil.getRooms();
			
		} catch (Exception exc) {
			// Send this to server logs
			logger.log(Level.SEVERE, "Error loading rooms", exc);

			// Add error message for JSF page
			addErrorMessage(exc);
			
		} 
		
	}
//	// ------------------------------------------------------------------------------

	public String addPanel(Panel thePanel) {

		logger.info("Adding panel: " + thePanel);

		try {

			// add student to the database
			DbUtil.addPanel(thePanel);

		} catch (Exception exc) {
			// send this to server logs
			logger.log(Level.SEVERE, "Error adding panel", exc);

			// add error message for JSF page
			addErrorMessage(exc);

			return null;
		}

		return "schedule?faces-redirect=true";
	}

	public String loadPanel(int panelId) {

		logger.info("loading panel: " + panelId);

		try {
			// get panel from database
			Panel panel = DbUtil.getPanel(panelId);
//		thePanel.getPresenterOptions();

			// put in the request attribute ... so we can use it on the form page
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();

			Map<String, Object> requestMap = externalContext.getRequestMap();
			requestMap.put("panel", panel);
			
			panel.getPresenterOptions();
			panel.getRoomOptions();

		} catch (Exception exc) {
			// send this to server logs
			logger.log(Level.SEVERE, "Error loading panel id:" + panelId, exc);

			// add error message for JSF page
			addErrorMessage(exc);

			return null;
		}
		return "edit_panel.xhtml";
	}
	
	public String loadPresenter(int presenterId) {

		logger.info("loading presenter: " + presenterId);

		try {
			// get student from database
			Presenter thePresenter = DbUtil.getPresenter(presenterId);

			// put in the request attribute ... so we can use it on the form page
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();

			Map<String, Object> requestMap = externalContext.getRequestMap();
			requestMap.put("presenter", thePresenter);

		} catch (Exception exc) {
			// send this to server logs
			logger.log(Level.SEVERE, "Error loading panel id:" + presenterId, exc);

			// add error message for JSF page
			addErrorMessage(exc);

			return null;
		}

		return "edit_presenter.xhtml";
	}

	public String updatePanel(Panel thePanel) {
		
		logger.info("updating panel: " + thePanel);
		

		try {

			// update student in the database
			DbUtil.updatePanel(thePanel);

		} catch (Exception exc) {
			// send this to server logs
			logger.log(Level.SEVERE, "Error updating panel: " + thePanel, exc);

			// add error message for JSF page
			addErrorMessage(exc);

			return null;
		}

		return "schedule?faces-redirect=true";
	}
	
	public String updatePresenter(Presenter thePresenter) {
		
		logger.info("updating presenter: " + thePresenter);
		

		try {
			// update presenter in the database
			DbUtil.updatePresenter(thePresenter);

		} catch (Exception exc) {
			// send this to server logs
			logger.log(Level.SEVERE, "Error updating presenter: " + thePresenter, exc);

			// add error message for JSF page
			addErrorMessage(exc);

			return null;
		}

		return "presenters_list?faces-redirect=true";
	}

	public String deletePanel(int panelId) {

		logger.info("Deleting panel id: " + panelId);

		try {

			// delete the student from the database
			DbUtil.deletePanel(panelId);

		} catch (Exception exc) {
			// send this to server logs
			logger.log(Level.SEVERE, "Error deleting panel id: " + panelId, exc);

			// add error message for JSF page
			addErrorMessage(exc);

			return null;
		}

		return "panel_list?";
	}

	public String deletePresenter(int presenterId) {

		logger.info("Deleting panel id: " + presenterId);

		try {

			// delete the student from the database
			DbUtil.deletePresenter(presenterId);

		} catch (Exception exc) {
			// send this to server logs
			logger.log(Level.SEVERE, "Error deleting presenter id: " + presenterId, exc);

			// add error message for JSF page
			addErrorMessage(exc);

			return null;
		}

		return "presenters_list?";
	}

	
	private void addErrorMessage(Exception exc) {
		FacesMessage message = new FacesMessage("Error: " + exc.getMessage());
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
	
	public static Controller getInstance() throws Exception {
		if (instance == null) {
			instance = new Controller();
		}

		return instance;
	}

}
