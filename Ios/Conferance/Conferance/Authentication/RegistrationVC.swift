//
//  RegistrationVC.swift
//  Conferance
//
//  Created by Sharra on 2019-04-05.
//  Copyright © 2019 Sharra. All rights reserved.
//

import UIKit

class RegistrationVC: UITableViewController {

    var user: User?
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPwd: UITextField!
    @IBOutlet weak var txtConfPwd: UITextField!
    
    @IBOutlet weak var lbError: UILabel!
    
    //error label triggered when invalid data submitted.
    @IBOutlet weak var Error: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    let itemsController1 = ItemsController()
    
    //detects if there is text in all of the required fields and activates the submit button
    @IBAction func textFieldChanged(_ sender: Any) {
        if txtName.hasText && txtEmail.hasText && txtPwd.hasText && txtConfPwd.hasText{
            btnSubmit.isEnabled = true
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //disable submit button by default
        btnSubmit.isEnabled=false
    
        
        //Call insert data to globals methods
        
        // code to edit user details
//        if let user = user{
//            txtName.text=user.name
//            txtEmail.text=user.email
//            if (txtPwd.text == txtConfPwd.text){
//                txtPwd.text = user.password
//        }
//        updateSubmitButtonState()
//    }
    }
    
    @IBAction func submitBtnPressed(_ sender: Any) {
        
        let name = txtName.text ?? ""
        let email = txtEmail.text ?? ""
        let password = txtPwd.text ?? ""
        let confPw = txtConfPwd.text ?? ""
        //resets background color of any possible error fields before revalidating
        txtName.backgroundColor = UIColor.white
        txtEmail.backgroundColor = UIColor.white
        txtPwd.backgroundColor = UIColor.white
        txtConfPwd.backgroundColor = UIColor.white
        
        //validates data and displays error label  & changes field form background color if failed validation
        //Otherwise submits data and segues to main logon screen
        if password == confPw{
            if email.isValidEmail() {
                
                if password.isValidPw(){
                
                gLuserObj = User(name: name, email: email, password: password)
                print("User object created")
                print(gLuserObj)
              //  var result = self.itemsController1.insertUser()
                var result = ""
                    if glUsers[gLuserObj.email] != nil{
                        result = "Duplicate ID"
                    }
                    else{
                        result = "Success"
                    }
                    
                switch result{
                case "Duplicate ID":
                    txtEmail.backgroundColor = UIColor.red
                    lbError.text="Email is already in Use!"
                case "Success":
                    glUsers[gLuserObj.email] = gLuserObj
                    let dataLoad = Data()
                    dataLoad.saveData()
                    performSegue(withIdentifier: "registered", sender: nil)
                    
                default:
                    showToast(message: "Eror")
                    }
                }
                else{
                    txtPwd.backgroundColor = UIColor.red
                    txtConfPwd.backgroundColor = UIColor.red
                    lbError.text="Password must have 8 - 13 Characters, 1 Uppercase Letter, 1 Lowercase Letter and 1 Number!"
                }
            }
            else {
                txtEmail.backgroundColor = UIColor.red
                lbError.text="Invalid email format. Must follow address@company.com"
            }
        }
        else {
            txtPwd.backgroundColor = UIColor.red
            txtConfPwd.backgroundColor = UIColor.red
            lbError.text="Passwords do not match!"
        }
        
        
        
        //pending api being up again
//        self.itemsController1.insertUser(){(completion: Bool? ) in
//            DispatchQueue.main.async{
//                print("return from inser req")
//                if completion == false{
//                    //display error indicating failure to create user due blah
//                    //refine later to various error codes that can be returned and how they display.
//                    self.showToast(message: "Completion False" )
//
//                }
//                else if completion == nil{
//                    // display error
//                    self.showToast(message: "Completion nil")
//                }
//                else{
//                    self.showToast(message: "Successful user insert")
//                    // call segue to main screen
//                    // Set user email to user email in seg
//                }
//
//            }
//
//        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //segue back to main and sets email field based of data entered on this screen.
        if segue.identifier == "registered"{
             if let targetVC = segue.destination as? logonVC{
                targetVC.userIDReg=gLuserObj.email
                
            }
        }
    }
    

    
}

extension String{
    //validation method to check email follows proper format
func isValidEmail() -> Bool {
    let regex = try! NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}", options: .caseInsensitive)
    //returns basrd off validation success or failure.
    return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
}
    //validates password requirements
    func isValidPw() -> Bool{
        
        let regex = try! NSRegularExpression(pattern: "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{7,13}$")
                //requires 1 upper, 1 lower, 1 number & 8 - 13 characters
        return regex.firstMatch(in: self, options:[], range: NSRange(location: 0, length: count)) != nil
        
    }
}

extension RegistrationVC {
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    } }
