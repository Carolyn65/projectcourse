package hellobobby.example.com.android_project.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import hellobobby.example.com.android_project.entities.Panel;
import hellobobby.example.com.android_project.R;
import hellobobby.example.com.android_project.entities.Schedule;
import hellobobby.example.com.android_project.services.Globals;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PanelPageActivity extends AppCompatActivity {

    ImageView iv_panelImage;
    TextView panelName;
    TextView panelPresenter;
    TextView panelDescription;
    TextView panelRoom;
    TextView panelTime;
    TextView panelRating;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panel_page);

        // get extras (the selected panel)
        Intent intent = getIntent();
        final Panel panel = (Panel)intent.getSerializableExtra("panel");
        //
        iv_panelImage = (ImageView) findViewById(R.id.iv_panelPageImage);
        panelName = (TextView) findViewById(R.id.tv_panelName);
        panelPresenter = (TextView) findViewById(R.id.tv_panelPresenter);
        panelDescription = (TextView) findViewById(R.id.tv_panelDescription);
        panelRoom = (TextView) findViewById(R.id.tv_panelRoom);
        panelTime = (TextView) findViewById(R.id.tv_panelTime);
        button = (Button) findViewById(R.id.bt_add_rm);
        //
        panelPresenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //PRESENTER PAGE STARTUP
                //TODO: Check if panel exists already
                Intent intent = new Intent(getApplicationContext(), PresenterPageActivity.class);
                intent.putExtra("presenter", panel.getPresenterObject());
                startActivity(intent);
            }
        });
        Call<ResponseBody> call = Globals.conventionAPI.getPresenterPhoto(panel.getPresenter());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        ResponseBody rb = response.body();
                        byte[] blobAsBytes = rb.bytes();
                        Bitmap btm = BitmapFactory.decodeByteArray(blobAsBytes, 0, blobAsBytes.length);
                        iv_panelImage.setImageBitmap(btm);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e("tag", "Error fetching Image 1");
                    Toast.makeText(getApplicationContext(), "Error fetching Image 1", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("tag", "Error fetching Image 2", t);
                Toast.makeText(getApplicationContext(), "Error fetching Image 2", Toast.LENGTH_SHORT).show();

            }
        });
        panelName.setText(panel.getName());
        panelPresenter.setText(panel.getPresenterObject().getName());
        panelDescription.setText(panel.getDescription());
        panelRoom.setText("Room: "+String.valueOf(panel.getRoom()));//FIXME change to pull the room description using get room description query
        panelTime.setText("When: "+panel.getDateTime().toString());
        String rating = "Rating: "+panel.getPanelRating()+"/5";
        iv_panelImage.setImageBitmap(panel.getPresenterObject().getPicture());
        //
        //FIXME: REMOVE FROM SCHEDULE!!! Needs to send authentication to work.
        for (Panel panel2: Globals.userSchedulePanels){
            if (panel.getId() == panel2.getId()){
                button.setText("Remove from Schedule");
//
//                for (Schedule schedule: UserScheduleActivity.full_schedule){
//                    if(schedule.getPanel_id()==panel.getId()){
//                        Call<Schedule> call = Globals.conventionAPI.deleteSchedule(schedule.getId());
//                        call.enqueue(new Callback<Schedule>() {
//                            @Override
//                            public void onResponse(Call<Schedule> call, Response<Schedule> response) {
//                                Toast.makeText(getApplicationContext(), "Removed", Toast.LENGTH_SHORT).show();
//                            }
//
//                            @Override
//                            public void onFailure(Call<Schedule> call, Throwable t) {
//                                Toast.makeText(getApplicationContext(), "Fail", Toast.LENGTH_SHORT).show();
//                            }
//                        });
//                    }
//                }
            }
        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Schedule schedule = new Schedule();
                schedule.setPanel_id(panel.getId());
                schedule.setUser_id(Globals.user.getId());

                Call<Schedule> addToUserSchedule = Globals.conventionAPI.setPeronalSchedule(schedule);
                addToUserSchedule.enqueue(new Callback<Schedule>() {
                    @Override
                    public void onResponse(Call<Schedule> call, Response<Schedule> response) {

                    }

                    @Override
                    public void onFailure(Call<Schedule> call, Throwable t) {

                    }
                });
            }
        });
        panelRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: print actual room name
                Intent intent = new Intent(getApplicationContext(), MapActivity.class);
                intent.putExtra(MainLoginActivity.EXTRA_ROOM, panel.getRoom());
                startActivity(intent);
            }
        });
    }
}
