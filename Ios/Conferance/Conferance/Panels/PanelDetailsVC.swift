//
//  PanelDetailsVC.swift
//  Conferance
//
//  Created by Sharra on 2019-04-05.
//  Copyright © 2019 Sharra. All rights reserved.
//

import UIKit

class PanelDetailsVC: UITableViewController {
    
    var panel: Panel?

    
    @IBOutlet weak var panelName: UILabel!
    @IBOutlet weak var room: UILabel!
    @IBOutlet weak var panelDescription: UITextView!
    @IBOutlet weak var when: UILabel!
    @IBOutlet weak var addToScheduleBtn: UIButton!
    @IBOutlet weak var guests: UIButton!
    
    
    var inSchedule = false
    
    //Item controller instance for using database
//    let itemsController1 = ItemsController()
    
        override func viewDidLoad() {
            super.viewDidLoad()
        }
    
    
    override func viewWillAppear(_ animated: Bool) {
        //set  data in fields
        panelName.text=panel?.name
        guests.setTitle(panel?.presenter.name, for: .normal)
        room.text=panel?.room.description
        panelDescription.text=panel?.description
        when.text=panel?.dateTime
        
        //check if in Userschedule and set flag
        if glUserPanels[panel!.id] != nil{
            inSchedule=true
        }
        else {
            inSchedule=false
        }
        //trigger button text based off boolean
        buttonState()
    }
    
    //return to previous view in the stack
    @IBAction func backBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //change button text based on if in schedule or not
    func buttonState(){
    if inSchedule{
    addToScheduleBtn.setTitle("Remove from MySchedule", for: .normal)
    }
    else{
    addToScheduleBtn.setTitle("Add to MySchedule", for: .normal)
    }
    }
    
    
    //Add or remove from schedule.
    //Updates saved file on disk accordingly and all instance variables.
    @IBAction func addToSchedule(_ sender: Any) {
        if inSchedule {
            glUserPanels.removeValue(forKey: panel!.id)
            inSchedule=false
            glUserSchedules[gLuserObj.email]?.userSchedule=glUserPanels
         
            buttonState()
            let dataLoad = Data()
            dataLoad.saveData()
        }
        else {
            glUserPanels[panel!.id] = panel
            inSchedule=true
            glUserSchedules[gLuserObj.email]?.userSchedule=glUserPanels
           
            buttonState()
            let dataLoad = Data()
            dataLoad.saveData()
            }
        
        
    }
    //Seg to details of the presenter
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "presenterDetail" {
            let navVC = segue.destination as! UINavigationController
            let detailViewController = navVC.viewControllers.first as! PresenterDetailsVC
            detailViewController.presenter=panel?.presenter
            
            
        }
 // If room details get added.
//        if segue.identifier == "roomDetail" {
//            let room = panel?.room
//
//            let navVC = segue.destination as! UINavigationController
//            let detailViewController = navVC.viewControllers.first as! UINavigationController
//            detailViewController.room = room
//        }
    }
    }



