//
//  LauchScreenVC.swift
//  Conferance
//
//  Created by Sharra on 2019-04-10.
//  Copyright © 2019 Sharra. All rights reserved.
//

import UIKit
import ImageIO

class LauchScreenVC: UIViewController {
    
    
    @IBOutlet weak var Spinner: UIImageView!
    var timer = Timer()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        let gif = UIImage.gifImageWithName("Spinner")
        Spinner.loadGif(name: "Spinner")
        
        timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.goToMainScreen), userInfo: nil, repeats: true)


    }

    func goToMainScreen() {
        
        timer.invalidate()
        
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "ViewController")
        
        
        self.navigationController?.pushViewController(nextVC!, animated: true)
    }
    

}

